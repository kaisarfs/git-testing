const express = require("express");
const app = express();

app.use(express.json());

app.listen(8001, () => console.log(`Listening @ http://localhost:8001`));

const router = require("./router/router");
app.use(router);
