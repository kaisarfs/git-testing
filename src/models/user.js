require("dotenv").config();

const mongoose = require("mongoose");
const joi = require("joi");

(async function () {
  try {
    await mongoose.connect(
      `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/user-service`
    );
    console.log("Connected to mongo");
  } catch (err) {
    console.log(err);
  }
})();

const userScema = new mongoose.Schema({
  nanoid: joi.string().required(),
  email: joi.string().email(),
  username: joi.string().alphanum().required(),
  password: joi.string().required(),
  createdAt: joi.string().required(),
  updatedAt: joi.string().required(),
});

const user = mongoose.model("users", userScema);

module.exports = user;
