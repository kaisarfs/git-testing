const { nanoid } = require("nanoid");
const bcrypt = require("bcrypt");
const user = require("../models/user");

async function find({ id, username, email }) {
  if (id) {
    return await user.findById(id);
  } else if (username) {
    return await user.findOne({
      username,
    });
  } else if (email) {
    return await user.findOne({
      email,
    });
  }
  return false;
}

async function findAllUser(fields) {
  if (fields) return await user.find({}).select(fields);
  else return await user.find();
}

async function createOneUser(email, username, plainPassword) {
  const password = await await bcrypt.hash(plainPassword, 10);
  const findUsername = await find({ username });
  const findEmail = await find({ email });

  async function create() {
    const createUser = await user.create({
      email,
      nanoid: nanoid(10),
      username,
      password: password,
      createdAt: Date.now(),
      updatedAt: Date.now(),
    });
    return {
      msg: `create user with username: '${createUser["username"]}' and _id: '${createUser["_id"]}'`,
      error: false,
    };
  }

  if (findUsername) {
    return { msg: `username '${findUsername["username"]}' already exists`, error: true };
  } else {
    const allUsername = await findAllUser("-_id username");
    if (allUsername.length > 0) {
      for (const uname of allUsername) {
        if (uname["username"].toLowerCase() == username.toLowerCase()) {
          return { msg: `cannot create user, please use another username`, error: true };
        } else if (findEmail) {
          return { msg: `email used by '${uname["username"]}'`, error: true };
        } else {
          return await create();
        }
      }
    } else {
      return await create();
    }
  }
}

async function deleteOneUser(id) {
  if (await find({ id })) {
    const deleted = await user.findByIdAndDelete(id);
    return { msg: `delete user '${deleted["username"]}'`, error: false };
  } else {
    return { msg: "user not found", error: true };
  }
}

async function patchOneUser({ id, email, username, plainPassword }) {
  let password;

  if (plainPassword) {
    password = await bcrypt.hash(plainPassword, 10);
  }

  if (await user.findById(id)) {
    const userData = await user.findByIdAndUpdate(id, {
      email,
      username,
      password,
      updatedAt: Date.now(),
    });
    return `change '${userData["username"]}' data`;
  } else {
    return false;
  }
}

const db = {
  createOneUser,
  deleteOneUser,
  patchOneUser,
  findAllUser,
};

module.exports = db;
