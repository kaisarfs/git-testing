module.exports = function ({ req, message, data, error }) {
  return {
    path: req.url,
    method: req.method,
    message,
    error,
    data,
  };
};
