const router = require("express").Router();
const db = require("../db/db");
const resTemplate = require("./resTemplate");

router.get("/", (req, res) => {
  res.json({
    date: new Date().toLocaleString("id-id").split(".").join(":").split("/").join("-"),
    message: "Hello World",
  });
});

router
  .route("/api/user")
  .post(async (req, res) => {
    const { email, username, password } = req.body;
    const created = await db.createOneUser(email, username, password);
    if (created) {
      res.json(
        resTemplate({
          req,
          message: created["msg"],
          error: created["error"],
        })
      );
    } else {
      res.json(
        resTemplate({
          req,
          message: created["msg"],
          error: created["error"],
        })
      );
    }
  })
  .patch(async (req, res) => {
    const { id, email, username, password } = req.body;
    let patched = await db.patchOneUser({
      id,
      username,
      email,
      plainPassword: password,
    });

    res.json(resTemplate({ req, error: false, message: patched }));
  })
  .delete(async (req, res) => {
    const { id } = req.body;
    const deleted = await db.deleteOneUser(id);
    if (deleted) {
      res.json(
        resTemplate({
          req,
          message: deleted["msg"],
          error: deleted["error"],
        })
      );
    } else {
      res.json(
        resTemplate({
          req,
          message: deleted["msg"],
          error: deleted["error"],
        })
      );
    }
  });

router.get("/api/user/all", async (req, res) => {
  res.json(
    resTemplate({
      req,
      message: "list all user",
      error: false,
      data: await db.findAllUser("_id nanoid username createdAt updatedAt"),
    })
  );
});
module.exports = router;
